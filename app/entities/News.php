<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="news")
 */
class News extends \Kdyby\Doctrine\Entities\IdentifiedEntity
{
	
	/**
	 * @ORM\Column(type="string")
	 */
	public $title;
	
	/**
	 * @ORM\Column(type="text")
	 */
	public $text;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	public $published;
	
}
