<?php

namespace App\Model;

use \Kdyby\Doctrine\EntityManager,
	\Kdyby\Doctrine\EntityDao,
	\Nette\Http\IRequest,
	\Kdyby\Doctrine\Entities\IdentifiedEntity;

class Facade extends \Nette\Object
{
	
	/**
	 * @var EntityManager
	 */
	private $entityManager;
	
	/**
	 * @var EntityDao
	 */
	private $dao;
	
	/**
	 * @var IRequest
	 */
	private $httpRequest;
	
	/**
	 * @var string
	 */
	private $entityName;

	/**
	 * @param EntityManager $entityManager
	 * @return void
	 */
	public function __construct(EntityManager $entityManager)
	{
		$this->entityManager = $entityManager;
	}

	/**
	 * @param string $entityName
	 * @param IRequest $httpRequest
	 * @param int $id
	 * @return \Nette\Object
	 * @throws \Nette\MemberAccessException
	 */
	public function getResponse($entityName, IRequest $httpRequest, $id = NULL)
	{
		$this->entityName = 'App\Entities\\' . $entityName;
		$this->dao = $this->entityManager->getDao($this->entityName);
		$this->httpRequest = $httpRequest;
		
		try {
			switch ($this->httpRequest->getMethod()) {
				case 'GET':
					return $this->get($id);
				case 'POST':
					return $this->create($this->getPost());
				case 'PUT':
					return $this->update($this->getPost());
				case 'DELETE':
					return $this->delete($id);
				default:
					throw new \Nette\MemberAccessException('Call to undefined action.');
			}
		} catch (\Kdyby\Doctrine\MemberAccessException $e) {
			return FALSE;
		}
	}
	
	private function call()
	{
		
	}
	
	/**
	 * @param int $id
	 * @return IdentifiedEntity
	 */
	public function get($id = NULL)
	{
		return ($id === NULL ? $this->dao->findAll() : $this->dao->find($id));
	}
	
	/**
	 * @param array $values
	 * @return IdentifiedEntity
	 */
	public function create($values)
	{
		return $this->saveEntity(new $this->entityName, $values);
	}
	
	/**
	 * @param array $values
	 * @return IdentifiedEntity
	 */
	public function update($values)
	{
		$entity = $this->get($values->id);
		unset($values->id);
		return $this->saveEntity($entity, $values);
	}
	
	/**
	 * @param int $id
	 * @return array
	 */
	public function delete($id)
	{
		$this->entityManager->remove($this->get($id));
		$this->entityManager->flush();
		return [];
	}
	
	/**
	 * @param IdentifiedEntity $entity
	 * @param array $values
	 * @return void
	 */
	private function setValues($entity, $values)
	{
		foreach ($values as $property => $value) {
			$this->setValue($entity, $property, $value);
		}
	}
	
	/**
	 * @param IdentifiedEntity $entity
	 * @param string $property
	 * @param string $value
	 * @return void
	 */
	private function setValue($entity, $property, $value)
	{
		$functionName = 'set' . ucfirst($property);
		$entity->$functionName($value);
	}
	
	/**
	 * @param IdentifiedEntity $entity
	 * @param array $values
	 * @return IdentifiedEntity
	 */
	private function saveEntity($entity, $values)
	{
		$this->entityManager->persist($entity);
		$this->setValues($entity, $values);
		$this->entityManager->flush();
		return $entity;
	}
	
	/**
	 * @return array
	 */
	public function getPost()
	{
		return \Nette\Utils\Json::decode($this->httpRequest->getRawBody());
	}
	
}
