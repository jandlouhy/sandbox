<?php

namespace App\Presenters;

abstract class BasePresenter extends \Nette\Application\UI\Presenter
{
	
	/**
	 * @var \App\Model\Facade
	 * @inject
	 */
	public $facade;

	public function actionRest($id = NULL)
	{
		$response = $this->facade->getResponse('News', $this->getHttpRequest(), $id);
		if ($response === FALSE) {
			$this->getHttpResponse()->setCode(\Nette\Http\Response::S500_INTERNAL_SERVER_ERROR);
			$response = [
				'error' => TRUE,
				'message' => 'Chyba',
			];
		}
		$this->sendJson($response);
	}
	
}
