$(function() {
	
	NewsModel = Backbone.Model.extend({
		urlRoot: basePath + '/admin/rest',
		defaults: {
			title: '',
			text: ''
		}
	});
	
	/*
	var news = new NewsModel({id: 1});
	news.fetch({
		success: function(news) {
			console.log(news.toJSON());
		},
		error: function() {
			console.error('Při načítání dat došlo k chybě.');
		}
	});
	*/
	
	var newsDetails = {
		title: 'Další test, tentokrát srkze REST',
		text: 'Hurá, je to tam!',
		blbost: 'asd'
	};
	var newsAdd = new NewsModel;
	newsAdd.save(newsDetails, {
		success: function(newsAdd) {
			console.log(newsAdd.toJSON());
		},
		error: function() {
			console.error('Při načítání dat došlo k chybě.');
		}
	});

	/*
	var newsDetails = {
		id: 2,
		title: 'a je to upraveno',
		text: 'jojo'
	};
	var newsAdd = new NewsModel;
	newsAdd.save(newsDetails, {
		success: function(newsAdd) {
			console.log(newsAdd.toJSON());
		},
		error: function() {
			console.error('Při načítání dat došlo k chybě.');
		}
	});
	*/
 
	/*
	var news = new NewsModel({id: 2});
	news.destroy({
		success: function() {
			console.log('destroyed');
		},
		error: function() {
			console.error('Při načítání dat došlo k chybě.');
		}
	});
	*/
	
});